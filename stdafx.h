// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <exception>
#include <algorithm>
#include <numeric>
#include <map>
#include <vector>
#include <regex>

// TODO: reference additional headers your program requires here

#include <GL/glew.h>
#include <GL/wglew.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include <fbxsdk.h>
#include <Magick++.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#define NOMINMAX
#include <windows.h>
