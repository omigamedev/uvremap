#include "stdafx.h"

#include "Rect.h"
#include "GuillotineBinPack.h"
#include "Primitive.h"

using namespace std;
//using namespace fbxsdk;
using namespace Magick;

// some useful range-loop implementation
namespace fbxsdk
{
    template <typename T>
    T* begin(FbxArray<T>& a) { return &a[0]; }
    template <typename T>
    T* end(FbxArray<T>& a) { return &a[a.GetCount() - 1]; }
    
    template <typename T>
    struct iter
    {
        FbxLayerElementArrayTemplate<T>* ar; int i;
        iter(FbxLayerElementArrayTemplate<T>* ar, int index) : ar(ar), i(index) { }
        bool operator!= (const iter<T>& other) const { return i != other.i; }
        const iter<T>& operator++ () { ++i; return *this; }
        T operator* () const { return ar->GetAt(i); };
    };
    template <typename T>
    iter<T> begin(FbxLayerElementArrayTemplate<T>& a) { return iter<T>(&a, 0); }
    template <typename T>
    iter<T> end(FbxLayerElementArrayTemplate<T>& a) { return iter<T>(&a, a.GetCount()); }
}

// some handy structs
struct UV
{
    int i;
    Coordinate c;
};
struct PolyFace
{
    int id;
    std::vector<UV> points;
    float worldArea;
    float uvArea;
    float density;
    glm::vec2 tiling;
};
struct Texture
{
    RectSize s;
    Rect r;
    Image img;
    std::string name;
    std::string title;
    std::vector<PolyFace> polys;
    glm::vec2 uvmin;
    glm::vec2 uvmax;
    glm::ivec2 uvgrid;
    int pixels;
    float worldArea;
    float uvArea;
    float density;
    bool resize;
};

// globals
const int g_width = 1024;
const int g_height = 1024;
const int g_pixels = g_width * g_height;
const int g_dilation = 10;

int main(int argc, char** argv)
{
    //glfwInit();
    //GLFWwindow* wnd = glfwCreateWindow(800, 600, "remapper", nullptr, nullptr);
    //glfwShowWindow(wnd);
    //glfwMakeContextCurrent(wnd);
    //glewInit();

    InitializeMagick(*argv);

    FbxManager* manager = FbxManager::Create();
    FbxScene* scene = FbxScene::Create(manager, "MyScene");
    
    // import phase
    FbxImporter* importer = FbxImporter::Create(manager, "");
    //importer->Initialize("cube.fbx");
    importer->Initialize("plant.fbx");
    int vMaj, vMin, vRev;
    importer->GetFileVersion(vMaj, vMin, vRev);
    printf("file version: %d.%d.%d\n", vMaj, vMin, vRev);
    importer->Import(scene);
    importer->Destroy(); 

    // let's just grab the first mesh
    FbxNode* root = scene->GetRootNode();
    FbxNode* node = root->GetChild(0);
    FbxMesh* mesh = node->GetMesh();

    std::string newname = std::string(node->GetName()) + "_merged";
    node->SetName(newname.c_str());
    
    FbxStringList uvSets;
    mesh->GetUVSetNames(uvSets);

    FbxVector4* cpoints = mesh->GetControlPoints();
    int* cpoints_idx = mesh->GetPolygonVertices();
    // FBX Control Points Example: C:\FBX-SDK\2017.0.1\samples\ProceduralTexture\main.cxx
    // see: http://download.autodesk.com/us/fbx/SDKdocs/FBX_SDK_Help/files/fbxsdkref/class_k_fbx_mesh.html#7633ddc21337e2bab781415d9bb39355
    auto getv = [&](int idx)
    {
        FbxVector4 p = cpoints[cpoints_idx[idx]];
        return glm::vec3(p[0], p[1], p[2]);
    };

    char newtex[256];
    char *newtex_title;
    GetFullPathName("plant_out.png", sizeof(newtex), newtex, &newtex_title);

    std::vector<glm::vec3> glmesh_vert;
    std::vector<glm::vec2> glmesh_uv;
    std::vector<GLuint> glmesh_idx;

    //auto* cp = mesh->GetControlPoints();
    //int ncp = mesh->GetControlPointsCount();
    //for (int i = 0; i < ncp; i++)
    //{
    //    auto v = cp[i];
    //    glmesh_vert.push_back(glm::vec3(v[0], v[1], v[2]));
    //}

    //for (int polyID = 0; polyID < mesh->GetPolygonCount(); polyID++)
    //{
    //    for (int pointID = 0; pointID < mesh->GetPolygonSize(polyID); pointID++)
    //    {

    //    }
    //}

    // parsing the scene
    std::map<std::string, Texture> tmap; // group textures into this map
    for (int elementMatID = 0; elementMatID < mesh->GetElementMaterialCount(); elementMatID++)
    {
        FbxGeometryElementMaterial* elementMat = mesh->GetElementMaterial(elementMatID);
        for (int matID = 0; matID < node->GetMaterialCount(); matID++)
        {
            FbxSurfaceLambert* smat = (FbxSurfaceLambert*)node->GetMaterial(matID);
            if (smat == nullptr) // don't support materials different than Lambert
                continue;

            printf("material: %s\n", smat->GetName());
            FbxProperty prop = smat->FindProperty(smat->sDiffuse);

            glm::vec2 tiling = { 1,1 };
            static char tex_filename[256];

            enum class tex_type_e { TEX, COLOR } tex_type;
            
            if (auto tex = prop.GetSrcObject<FbxFileTexture>())
            {
                tiling = glm::vec2(tex->Scaling.Get()[0], tex->Scaling.Get()[1]);
                strcpy_s(tex_filename, tex->GetFileName());
                tex_type = tex_type_e::TEX;
            }
            else
            {
                tex_type = tex_type_e::COLOR;
                auto col = smat->Diffuse.Get();
                int r = (int)std::floor(col[0] * 256);
                int g = (int)std::floor(col[1] * 256);
                int b = (int)std::floor(col[2] * 256);
                sprintf_s(tex_filename, "COL%d%d%d", r, g, b);
            }

            printf("diffuse (tiling %f,%f): \"%s\"\n", tiling.x, tiling.y, tex_filename);

            if (tmap.find(tex_filename) == tmap.end())
            {
                Texture& t = tmap[tex_filename];
                if (tex_type == tex_type_e::TEX)
                {
                    // split path apart
                    std::cmatch matches;
                    std::regex_search(tex_filename, matches, std::regex{ R"|(^(\w:[\\/].*/)([^/\\\.]+)\.(\w+)$)|" });
                    auto dir = matches[1].str();
                    auto name = matches[2].str();
                    auto ext = matches[3].str();
                    t.title = name;
                    t.img.read(tex_filename);
                    t.resize = true;
                }
                else
                {
                    t.title = tex_filename;

                    auto col = smat->Diffuse.Get();
                    int r = (int)std::floor(col[0] * 256);
                    int g = (int)std::floor(col[1] * 256);
                    int b = (int)std::floor(col[2] * 256);
                    t.img = Image(Geometry(32, 32), Color(r, g, b, 255));
                    t.resize = false;
                }

                // not found, so load it
                t.name = tex_filename;
                t.s = { (int)t.img.size().width(), (int)t.img.size().width() };
                t.worldArea = 0;
                t.density = 0;
                t.pixels = t.img.size().width() * t.img.size().height();
                t.uvmin = glm::vec2{ std::numeric_limits<float>::max() };
                t.uvmax = glm::vec2{ std::numeric_limits<float>::min() };
            }

            Texture& t = tmap[tex_filename];
            std::vector<glm::vec2> poly_uvs;

            for (int polyID = 0; polyID < mesh->GetPolygonCount(); polyID++)
            {
                if (elementMat->GetIndexArray().GetAt(polyID) != matID)
                    continue;
 
                std::vector<UV> poly_coord;
                std::vector<glm::vec2> points;
                std::vector<glm::vec2> uvs;
                int vert_index = mesh->GetPolygonVertexIndex(polyID);
                int polySize = mesh->GetPolygonSize(polyID);

                // temp sum to calculate the mid point
                glm::vec2 points_sum{ 0.f };
                glm::vec2 uvs_sum{ 0.f };

                // polygon first 3 vertex
                glm::vec3 v0 = getv(vert_index + 0);
                glm::vec3 v1 = getv(vert_index + 1);
                glm::vec3 v2 = getv(vert_index + 2);
                // intermediate values to project on 2D
                auto loc0 = v0;                             // local origin
                auto locx = glm::normalize(v1 - loc0);      // local X axis
                auto normal = glm::cross(locx, v2 - loc0);  // vector orthogonal to polygon plane
                auto locy = glm::normalize(glm::cross(normal, locx));       // local Y axis

                for (int pointID = 0; pointID < mesh->GetPolygonSize(polyID); pointID++)
                {
                    FbxVector2 uv;
                    bool unmapped; // unused
                    mesh->GetPolygonVertexUV(polyID, pointID, uvSets[0], uv, unmapped);
                    poly_coord.push_back({ vert_index + pointID, Coordinate(uv[0], uv[1]) });
                        
                    // project onto 2D plane
                    glm::vec3 v = getv(vert_index + pointID);
                    glm::vec2 p = { glm::dot(v - loc0, locx), glm::dot(v - loc0, locy) };
                    // todo: to optimize, make an ordered array insertion instead of a late ordering
                    points.push_back(p);
                    points_sum += p; // to calculate the mid point

                    // save uv coordinates to compute pixel density
                    uvs.push_back({ uv[0], uv[1] });
                    uvs_sum += glm::vec2{ uv[0], uv[1] }; // to calculate the mid point

                    poly_uvs.push_back({ uv[0], uv[1] });

                    //printf("poly %d vertex %d uv %f %f pos %f %f %f\n", polyID, i, uv[0], uv[1], v0.x, v0.y, v0.z);
                }
                //poly_coord.push_back(poly_coord[0]); // close the poly

                // sort using polar coordinates from center
                auto center = points_sum / (float)polySize;
                std::sort(points.begin(), points.end(), [center](glm::vec2 p0, glm::vec2 p1)
                {
                    auto a = p0 - center;
                    auto b = p1 - center;
                    return atan2(b.y, b.x) - atan2(a.y, a.x) >= 0;
                });
                auto uvs_center = uvs_sum / (float)polySize;
                std::sort(uvs.begin(), uvs.end(), [uvs_center](glm::vec2 p0, glm::vec2 p1)
                {
                    auto a = p0 - uvs_center;
                    auto b = p1 - uvs_center;
                    return atan2(b.y, b.x) - atan2(a.y, a.x) >= 0;
                });

                // compute the area of each polygon in world-space
                // in order to weight the uv space partition
                // see: https://en.wikipedia.org/wiki/Shoelace_formula
                float area = 0;
                for (unsigned int pointID = 0; pointID < points.size(); pointID++)
                {
                    glm::vec2 p0 = points[pointID];
                    glm::vec2 p1 = points[(pointID + 1) % polySize];
                    area += p0.x * p1.y - p0.y * p1.x;
                }
                area *= 0.5f;

                // compute UV-space area
                float uv_area = 0;
                for (unsigned int uvID = 0; uvID < uvs.size(); uvID++)
                {
                    glm::vec2 p0 = uvs[uvID];
                    glm::vec2 p1 = uvs[(uvID + 1) % polySize];
                    uv_area += p0.x * p1.y - p0.y * p1.x;
                }
                uv_area *= 0.5f;

                //printf("poly %d area %f uv area %f\n", polyID, area, uv_area);

                if (uv_area > 0.0001 && area > 0.0001)
                {
                    float density = std::sqrtf(area / (uv_area * tiling.x * tiling.y));
                    t.polys.push_back(PolyFace{ polyID, std::move(poly_coord), area, uv_area, density, tiling });
                    t.worldArea = std::max(t.worldArea, area);
                    t.uvArea = std::max(t.uvArea, uv_area);
                    t.density = std::max(t.density, density);
                }
                else
                {
                    //printf("Skipped ZERO area polygon\n");
                }
            }

            // compute uv bounds
            auto bb_min = [](glm::vec2 a, glm::vec2 b) { return glm::vec2(std::min(a.x, b.x), std::min(a.y, b.y)); };
            auto bb_max = [](glm::vec2 a, glm::vec2 b) { return glm::vec2(std::max(a.x, b.x), std::max(a.y, b.y)); };
            auto bbox = [&](const std::vector<glm::vec2>& v)
            {
                glm::vec2 min{ std::numeric_limits<float>::max() };
                glm::vec2 max{ std::numeric_limits<float>::min() };
                for (auto& p : v)
                {
                    min = bb_min(min, p);
                    max = bb_max(max, p);
                }
                return std::pair<glm::vec2, glm::vec2>(min, max);
            };

            glm::vec2 uvmin, uvmax;
            std::tie(uvmin, uvmax) = bbox(poly_uvs);
            t.uvmin = bb_min(t.uvmin, uvmin);
            t.uvmax = bb_max(t.uvmax, uvmax);
            // todo: check this, this is probably not going to work in some instances
            t.uvgrid = glm::round(t.uvmin);
        }
    }

    std::vector<Texture> textures;
    for (auto& m : tmap)
        textures.push_back(std::move(m.second));

    float tot_density = std::accumulate(textures.begin(), textures.end(), 0.f,
        [](float lhs, const Texture& rhs) { return lhs + rhs.density; });

    // calculate and print initial area 
    for (auto& t : textures)
    {
        if (t.resize)
        {
            float ratio = t.density / tot_density;
            float img_ratio = (float)t.pixels / (float)g_pixels;
            t.s.width = (int)std::floor(t.s.width * ratio * 20.f);
            t.s.height = (int)std::floor(t.s.height * ratio * 20.f);
            printf("img %4f - density %4f%% for: \"%s\"\n", img_ratio, ratio, t.title.c_str());
        }
    }

    // sort by size
    std::sort(textures.begin(), textures.end(), [](Texture const & a, Texture const & b)
    { return (a.s.width * a.s.height) > (b.s.width * b.s.height); });

    // remove all of the old materials assignment
    while (mesh->GetElementMaterialCount())
        mesh->RemoveElementMaterial(mesh->GetElementMaterial(0));

    // clear the scene from the old materials
    while (scene->GetMaterialCount())
        scene->RemoveMaterial(scene->GetMaterial(0));

    // create a new merged material and texture
    FbxSurfaceLambert* mergedmat = FbxSurfaceLambert::Create(manager, "MergedMaterial");
    FbxFileTexture* mergedtex = FbxFileTexture::Create(manager, "MergedTexture");
    mergedtex->SetFileName(newtex);
    mergedmat->Diffuse.ConnectSrcObject(mergedtex);
    node->AddMaterial(mergedmat);
    scene->AddTexture(mergedtex);

    // Packing phase
    bool done = false;
    while (!done)
    {
        // see: https://github.com/juj/RectangleBinPack
        // see: http://clb.demon.fi/projects/even-more-rectangle-bin-packing
        GuillotineBinPack bin(g_width, g_height); // Stores the current packing progress.

        int numPacked = 0; // The number of rectangles we packed successfully, for printing statistics at the end.
        for (auto& t : textures)
        {
            // Pack the next rectangle in the input list.
            t.r = bin.Insert(t.s.width + g_dilation * 2, t.s.height + g_dilation * 2,
                true, // Use the rectangle merge heuristic rule
                GuillotineBinPack::RectBestShortSideFit,
                GuillotineBinPack::SplitMinimizeArea);

            // If the packer returns a degenerate height rect, it means the packing failed.
            if (t.r.height == 0)
                break;
            else // succeeded.
                ++numPacked;
        }

        done = numPacked == textures.size();
        if (!done)
        {
            // reduce all the textures size
            for (auto& t : textures)
            {
                if (t.resize)
                {
                    t.s.width = std::max(128, (int)std::floor(t.s.width * .99f));
                    t.s.height = std::max(128, (int)std::floor(t.s.height * .99f));
                }
            }
        }
    }

    // composite the final texture and set the new UVs
    try
    {
        Image image(Geometry(g_width, g_height), Color(0, 0, 0, 0));
        auto* eluv = mesh->GetElementUV(uvSets[0]);
        auto eluv_map = eluv->GetIndexArray();

        auto* elcolor = mesh->GetElementVertexColor();
        if (!elcolor)
        {
            elcolor = mesh->CreateElementVertexColor();
            elcolor->SetMappingMode(FbxGeometryElement::eByPolygonVertex);
            elcolor->SetReferenceMode(FbxGeometryElement::eIndexToDirect);
            elcolor->GetDirectArray().SetCount(eluv->GetDirectArray().GetCount());
            for (auto el : eluv->GetIndexArray())
                elcolor->GetIndexArray().Add(el);
        }
        auto elcolor_eluv_map = elcolor->GetIndexArray();

        // create a brand new uv set for texture box start
        auto* bbstart_eluv = mesh->CreateElementUV("box_start");
        bbstart_eluv->SetMappingMode(FbxGeometryElement::eByPolygonVertex);
        bbstart_eluv->SetReferenceMode(FbxGeometryElement::eIndexToDirect);
        bbstart_eluv->GetDirectArray().SetCount(eluv->GetDirectArray().GetCount());
        for (auto el : eluv->GetIndexArray())
            bbstart_eluv->GetIndexArray().Add(el);
        auto bbstart_eluv_map = bbstart_eluv->GetIndexArray();

        // create a brand new uv set for texture box extent
        auto* bbextent_eluv = mesh->CreateElementUV("box_extent");
        bbextent_eluv->SetMappingMode(FbxGeometryElement::eByPolygonVertex);
        bbextent_eluv->SetReferenceMode(FbxGeometryElement::eIndexToDirect);
        bbextent_eluv->GetDirectArray().SetCount(eluv->GetDirectArray().GetCount());
        for (auto el : eluv->GetIndexArray())
            bbextent_eluv->GetIndexArray().Add(el);
        auto bbextent_eluv_map = bbextent_eluv->GetIndexArray();

        for (auto& t : textures)
        {
            float x0 = (float)t.r.x + g_dilation;
            float y0 = (float)t.r.y + g_dilation;
            float x1 = (float)(t.r.x + t.r.width - g_dilation);
            float y1 = (float)(t.r.y + t.r.height - g_dilation);
            float w = (float)t.r.width - g_dilation * 2;
            float h = (float)t.r.height - g_dilation * 2;

            printf("compositing %s\n", t.title.c_str());
            //image.draw(DrawableCompositeImage(x0-g_dilation, y0-g_dilation, w+g_dilation*2, h+g_dilation*2, t.img, CompositeOperator::CopyCompositeOp));
            image.draw(DrawableCompositeImage(x0, y0, w, h, t.img, CompositeOperator::CopyCompositeOp));

            auto s = t.img.size();
            for (int i = 0; i < g_dilation; i++)
            {
                image.copyPixels(t.img, Magick::Geometry(1, s.height(), s.width() - 1, 0), Magick::Offset(x1 + i, y1));
                image.draw(drawable)
            }

            // draw polygons
            image.fillPattern(Image());
            image.fillColor(Color(0, 255, 0, 0));     // change alpha to visualize
            image.strokeColor(Color(0, 0, 255, 255)); // change alpha to visualize
            image.strokeWidth(1);
            image.strokeAntiAlias(false);
            for (auto& poly : t.polys)
            {
                // relocate coordinates
                for (auto& uv : poly.points)
                {
                    float nx0 = x0 / g_width;
                    float ny0 = y0 / g_height;
                    float nw = w / g_width;
                    float nh = h / g_height;
                    //float u = nx0  + (uv.c.x() - t.uvgrid.x) * nw;
                    //float v = ny0 + (uv.c.y() - t.uvgrid.y) * nh;
                    float u = nx0 + uv.c.x() * nw;
                    float v = ny0 + uv.c.y() * nh;

                    // note: vertex color is clamped, so divide by N and then multiply by N in the shader
                    elcolor->GetDirectArray().SetAt(elcolor_eluv_map[uv.i], FbxVector4(poly.tiling.x * .1f, poly.tiling.y * .1f, 0, 0));
                    bbstart_eluv->GetDirectArray().SetAt(bbstart_eluv_map[uv.i], FbxVector2(nx0, 1 - ny0 - nh));
                    bbextent_eluv->GetDirectArray().SetAt(bbextent_eluv_map[uv.i], FbxVector2(nw, nh));
                }
            }
        }

        if (1)
        {
            Image lightmap;
            printf("Compositing lightmap to alpha channel");
            lightmap.read(R"!(D:\Omar\Documents\Substance Painter 2\export\PowerPlant_Substance_lambert1_AmbientOcclusion.png)!");
            //lightmap.channel(ChannelType::RedChannel);
            image.draw(DrawableCompositeImage(0, 0, g_width, g_height, lightmap, CompositeOperator::CopyAlphaCompositeOp));
        }

        image.write(newtex);

        // Export phase
        printf("exporting...\n");
        FbxExporter* exporter = FbxExporter::Create(manager, "");

        // Write in fall back format in less no ASCII format found
        auto pFileFormat = manager->GetIOPluginRegistry()->GetNativeWriterFormat();

        //Try to export in ASCII if possible
        int lFormatIndex, lFormatCount = manager->GetIOPluginRegistry()->GetWriterFormatCount();

        for (lFormatIndex = 0; lFormatIndex < lFormatCount; lFormatIndex++)
        {
            if (manager->GetIOPluginRegistry()->WriterIsFBX(lFormatIndex))
            {
                FbxString lDesc = manager->GetIOPluginRegistry()->GetWriterFormatDescription(lFormatIndex);
                const char *lASCII = "ascii";
                if (lDesc.Find(lASCII) >= 0)
                {
                    pFileFormat = lFormatIndex;
                    break;
                }
            }
        }

        exporter->Initialize("plant_export.fbx", pFileFormat);
        if (!exporter->Export(scene))
            printf("failed to export the FBX\n");
        exporter->Destroy();
    }
    catch (exception &error_)
    {
        cout << "Caught exception: " << error_.what() << endl;
        system("pause");
        return 1;
    }
    
    system("pause");

    return 0;
}
