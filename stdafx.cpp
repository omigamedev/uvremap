// stdafx.cpp : source file that includes just the standard includes
// remap.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

#pragma comment (lib, "CORE_RL_Magick++_.lib")
#pragma comment (lib, "libfbxsdk-md.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "OpenGL32.lib")
